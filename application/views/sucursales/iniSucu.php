      <!-- our birds -->
      <div class="birds">
          <div class="container">
              <div class="row">
                  <div class="col-md-7">
                      <div class="titlepage">
                          <h2>Nuestras Sucursales</h2>
                          <p> Una vista a la las sucursales que existen dentro y fuera del país</p>
                      </div>
                  </div>
              </div>

              <div class="row">
                  <!-- slider bird1 -->
                  <div class="col-md-7 padding_01">
                      <div id="bird1" class="carousel slide our_birds" data-ride="carousel">
                          <ol class="carousel-indicators">
                              <li data-target="#bird1" data-slide-to="0" class="active"></li>
                              <li data-target="#bird1" data-slide-to="1"></li>
                              <li data-target="#bird1" data-slide-to="2"></li>
                          </ol>
                          <div class="carousel-inner">
                              <div class="carousel-item active">
                                  <img class="first-slide" src="<?php echo base_url(); ?>assets/images/sucu01.png" alt="First slide">
                                  <div class="container">
                                      <div class="carousel-caption positi_abso">
                                      </div>
                                  </div>
                              </div>
                              <div class="carousel-item">
                                  <img class="second-slide" src="<?php echo base_url(); ?>assets/images/sucu02.png" alt="Second slide">
                                  <div class="container">
                                      <div class="carousel-caption positi_abso">
                                      </div>
                                  </div>
                              </div>
                              <div class="carousel-item">
                                  <img class="third-slide" src="<?php echo base_url(); ?>assets/images/sucu03.png" alt="Third slide">
                                  <div class="container">
                                      <div class="carousel-caption positi_abso">
                                      </div>
                                  </div>
                              </div>
                              <div class="carousel-item">
                                  <img class="third-slide" src="<?php echo base_url(); ?>assets/images/sucu04.png" alt="Third slide">
                                  <div class="container">
                                      <div class="carousel-caption positi_abso">
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <a class="carousel-control-prev" href="#bird1" role="button" data-slide="prev">
                              <i class="fa fa-angle-left" aria-hidden="true"></i>
                              <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#bird1" role="button" data-slide="next">
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                              <span class="sr-only">Next</span>
                          </a>
                      </div>
                  </div>
                  <!-- end slider bird1 -->
                  <!-- slider bird2 -->
                  <div class="col-md-5 padding_01">
                      <div id="bird2" class="carousel slide our_birds" data-ride="carousel">
                          <ol class="carousel-indicators">
                              <li data-target="#bird2" data-slide-to="0" class="active"></li>
                              <li data-target="#bird2" data-slide-to="1"></li>
                              <li data-target="#bird2" data-slide-to="2"></li>
                          </ol>
                          <div class="carousel-inner">
                              <div class="carousel-item active">
                                  <img class="first-slide" src="<?php echo base_url(); ?>assets/images/sucu11.png" alt="First slide">
                                  <div class="container">
                                      <div class="carousel-caption positi_abso">
                                      </div>
                                  </div>
                              </div>
                              <div class="carousel-item">
                                  <img class="second-slide" src="<?php echo base_url(); ?>assets/images/sucu12.png" alt="Second slide">
                                  <div class="container">
                                      <div class="carousel-caption positi_abso">
                                      </div>
                                  </div>
                              </div>
                              <div class="carousel-item">
                                  <img class="third-slide" src="<?php echo base_url(); ?>assets/images/sucu13.png" alt="Third slide">
                                  <div class="container">
                                      <div class="carousel-caption positi_abso">
                                      </div>
                                  </div>
                              </div>
                              <div class="carousel-item">
                                  <img class="third-slide" src="<?php echo base_url(); ?>assets/images/sucu14.png" alt="Third slide">
                                  <div class="container">
                                      <div class="carousel-caption positi_abso">
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <a class="carousel-control-prev" href="#bird2" role="button" data-slide="prev">
                              <i class="fa fa-angle-left" aria-hidden="true"></i>
                              <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#bird2" role="button" data-slide="next">
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                              <span class="sr-only">Next</span>
                          </a>
                      </div>
                      <a class="read_more" style="float: right; margin-top: 60px;" href="<?php echo site_url(); ?>/sucursales/agreSucu">Agregar nuevo</a>
                      <br><br><br>
                      <a class="read_more" style="float: right; margin-top: 60px;" href="<?php echo site_url(); ?>/sucursales/listarSucu">Listado</a>

                  </div>

                  <!-- end slider bird2 -->
              </div>
          </div>
      </div>
      <!-- end our birds -->