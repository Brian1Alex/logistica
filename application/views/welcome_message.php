<div class=" banner_main">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-5">
                  <div class="bluid">
                     <h1>La mejor <br><span class="black">Opcion <br>en Logistica</span></h1>
                     <p>Bienvenido<br> confie sus envios a la red mas grande en logistica del mundo</p>
                     <a class="read_more" href="<?php echo site_url() ?>/sucursales/iniSucu">Sucursales</a>
                  </div>
               </div>
               <div class="col-md-7">
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                     <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                     </ol>
                     <div class="carousel-inner">
                        <div class="carousel-item active">
                           <div class="container">
                              <div class="carousel-caption relative">
                                 <div class="banner_img">
                                    <figure><img src="<?php echo base_url();?>/assets/images/sucu03.png" alt="#"/></figure>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="carousel-item">
                           <div class="container">
                              <div class="carousel-caption relative">
                                 <div class="banner_img">
                                    <figure><img src="<?php echo base_url();?>/assets/images/fondo2.png" alt="#"/></figure>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="carousel-item">
                           <div class="container">
                              <div class="carousel-caption relative">
                                 <div class="banner_img">
                                    <figure><img src="<?php echo base_url();?>/assets/images/fondo.png" alt="#"/></figure>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="carousel-item">
                           <div class="container">
                              <div class="carousel-caption relative">
                                 <div class="banner_img">
                                    <figure><img src="<?php echo base_url();?>/assets/images/fondo3.png" alt="#"/></figure>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                     <i class="fa fa-angle-left" aria-hidden="true"></i>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                     <i class="fa fa-angle-right" aria-hidden="true"></i>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>