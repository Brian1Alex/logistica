<!-- footer -->
<footer>
         <div class="footer">
            <div class="container">
               <div class="row">
                  <div class="col-md-10 offset-md-1 border-top1">
                  </div>
                  <div class="col-md-5">
                     <div class="reader">
                        <h3>@FEDEX</h3>
                        <ul>
                           <li>Calidad y eficiencia<br>
                              a tu alcance
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-5 col-md-5">
                     <div class="reader">
                        <h3>Conoce mas de <br>nosotros!</h3>
                     </div>
                  </div>
                  <div class="col-lg-2 col-md-3 ">
                     <div class="reader">
                        <h3>Gracias por <br>preferirnos! </h3>
                        <ul>
                           <li> <a href="Javascript:void(0)">No olvides que Fedex siempre esta disponible<br>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-10 offset-md-1">
                     <ul class="social_icon text_align_center">
                        <li> <a href="https://www.facebook.com/FedExLATAM/"><i class="fa fa-facebook-f"></i></a></li>
                        <li> <a href="https://twitter.com/fedexteayuda"><i class="fa fa-twitter"></i></a></li>
                        <li> <a href="https://www.instagram.com/fedex/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="copyright text_align_center">
               <div class="container">
                  <div class="row">
                     <div class="col-md-10 offset-md-1">
                        <p>Created By <a href="https://html.design/"> Bryan Cornejo and</a> Andy Cumbajin UTC 2023</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- end footer -->
      <!-- Javascript files-->
      <script src="<?php echo base_url(); ?>plant/js/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>plant/js/bootstrap.bundle.min.js"></script>
      <script src="<?php echo base_url(); ?>plant/js/jquery-3.0.0.min.js"></script>
      <script src="<?php echo base_url(); ?>plant/js/custom.js"></script>


      <!-- Importando JQuerry -->
      <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>


      <!-- Importando JQuerry Validate -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

      <!-- API Key de GoogleMaps-->
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoObNZz2rn6AMxGvMKq1GDTFvd7CzGwdY&libraries=places&callback=initMap"></script>


   </body>
</html>