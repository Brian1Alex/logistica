<div class="testimonial">
         <div class="container">
            <div class="row">
               <div class="col-md-9 offset-md-3">
                  <div class="titlepage text_align_right">
                     <h2>ENVIOS :D</h2>
                     <span>La rapidez y eficacia de nuestros servicios nos caracterizan como <br> 
                     institucion buena y confiable para todos nuestros clientes. </span>
                  </div>
               </div>
            </div>
            <div class="row d_flex">
               <div class="col-md-7">
                  <div id="testimo" class="carousel slide our_testimonial" data-ride="carousel">
                     <ol class="carousel-indicators">
                        <li data-target="#testimo" data-slide-to="0" class="active"></li>
                        <li data-target="#testimo" data-slide-to="1"></li>
                        <li data-target="#testimo" data-slide-to="2"></li>
                     </ol>
                     <div class="carousel-inner">
                        <div class="carousel-item active">
                           <div class="container">
                              <div class="carousel-caption posi_in">
                                 <div class="testomoniam_text">
                                    <i><img src="<?php echo base_url(); ?>/assets/images/camionsito.gif" class="services_img"></i>
                                    <h3>AGREGAR</h3>
                                    <span>Envio</span>
                                    <p>Asigna nuevos envios para enlistar.</p>
                                    <a class="read_more" style="float: center; margin-top: 60px;" href="<?php echo site_url(); ?>/pedidos/agrePedi">AGREGAR</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="carousel-item">
                           <div class="container">
                              <div class="carousel-caption posi_in">
                                 <div class="testomoniam_text">
                                    <i><img src="<?php echo base_url(); ?>/assets/images/camionsito.gif" class="services_img"></i>
                                    <h3>LISTAR</h3>
                                    <span>Envio</span>
                                    <p>Lista de los envios previamente registrados</p>
                                    <a class="read_more" style="float: center; margin-top: 60px;" href="<?php echo site_url(); ?>/pedidos/listarPedi">  LISTAR</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="carousel-item">
                           <div class="container">
                              <div class="carousel-caption posi_in">
                                 <div class="testomoniam_text">
                                    <i><div><img src="<?php echo base_url(); ?>/assets/images/celu.png" class="services_img"></i>
                                    <h3>DESTINOS</h3>
                                    <span>Envios</span>
                                    <p>Ubicaciones preregistradas mostradas en Mapa</p>
                                    <a class="read_more" style="float: center; margin-top: 60px;" href="<?php echo site_url(); ?>/pedidos/desPedi">DESTINOS</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <a class="carousel-control-prev" href="#testimo" role="button" data-slide="prev">
                     <i class="fa fa-angle-left" aria-hidden="true"></i>
                     <span class="sr-only">Previous</span>
                     </a>
                     <a class="carousel-control-next" href="#testimo" role="button" data-slide="next">
                     <i class="fa fa-angle-right" aria-hidden="true"></i>
                     <span class="sr-only">Next</span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>