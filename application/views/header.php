<!DOCTYPE html>
<html lang="en">

<head>
   <!-- basic -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <!-- mobile metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1">
   <!-- site metas -->
   <title>Logistica</title>
   <meta name="keywords" content="">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- bootstrap css -->
   <link rel="stylesheet" href="<?php echo base_url('plant/css/bootstrap.min.css'); ?>">
   <!-- style css -->
   <link rel="stylesheet" href="<?php echo base_url('plant/css/style.css'); ?>">
   <!-- responsive-->
   <link rel="stylesheet" href="<?php echo base_url('plant/css/responsive.css'); ?>">
   <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo2.png" type="image/gif">
   <!-- awesome fontfamily -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

   <!-- Importando JQuerry -->
   <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>

   <!-- Importando JQuerry Validate -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
   <script type="text/javascript">
      jQuery.validator.addMethod("letras", function(value, element) {
         //return this.optional(element) || /^[a-z]+$/i.test(value);
         return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú]*$/.test(value);

      }, "Este campo solo acepta letras");
   </script>
</head>
<!-- body -->

<body class="main-layout">
   <!-- loader  -->
   <div class="loader_bg">
      <div class="loader"><img src="<?php echo base_url('plant/images/loading.gif'); ?>" alt="" /></div>
   </div>
   <!-- end loader -->
   <div id="mySidepanel" class="sidepanel">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
      <a class="active" href="<?php echo site_url(); ?>">Home</a>
      <a href="<?php echo site_url(); ?>/sucursales/iniSucu">Nuestras Sucursales</a>
      <a href="<?php echo site_url() ?>/contactanos/iniCon">Contactos</a>
   </div>
   <!-- header -->
   <header>
      <!-- header inner -->
      <div class="head-top">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-3">
                  <div class="logo">
                     <a href="<?php echo site_url(); ?>"><img src="<?php echo base_url('assets/images/logo.png'); ?>" width="128px" /></a>
                  </div>
               </div>
               <div class="col-sm-9">
                  <ul class="email text_align_right">
                     <li class="d_none"> <a href="<?php echo site_url() ?>/clientes/iniClie">Clientes</a> </li>
                     <li class="d_none"> <a href="<?php echo site_url() ?>/pedidos/iniPedi">Envios</a> </li>
                     <li> <button class="openbtn" onclick="openNav()"><img src="<?php echo base_url('plant/images/menu_btn.png'); ?>"></button></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </header>
   <!-- end header -->